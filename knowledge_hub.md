# **Knowledge Hub**

## **What is this?**

Under the request of SAP, our group did research into technologies we could potentially use in the creation of the project. Most of this research was done long before the creation of this document, however our group decided in private communication channels that we needed a place to collect the information that we had collected that was easy for our tutor and the shadows to access. This document was created with the intent for the shadows and our tutor to be able to easily review our research.

## **Caseworker Experience (Sijiang Guo):**

Duplicated from: https://gitlab.cecs.anu.edu.au/u5804087/NPF/issues/10

*  The user experience of a caseworker includes following aspects:

1.  System's overall performance (Searching, loading and saving speed; recording speed by hand writing).
2.  System's accessibility (Anywhere access).
3.  Comprehensive and well-organised UI/forms. 

*  These aspects have to be considered for all operating processes.

1. Creating the profile of a new client while having a face-to-face meeting.
   *  Mnimal necessary boxes to fill for first meeting.
   *  Support multiple electronical devices.
   *  Or simple and clear paper form
2. Creating the profile of a new client while having a phone call.
   *  Phonecall recording if allowed.
   *  Fast access to electronical systems on tablet/laptop.
   *  Simplified special hard paper card for temperary paper recording (cards are easier to take), when electronical devices are not available.
3. Finding the profile of certain clients in the system.
   *  The first page seen in the application after logging in must be the name list of all clients, clearly showing the date of creation of each profile. 
   *  The first page must include search box. 
   *  The list allows sorting by alphabetics, time of creation and district (if available).
4. Obtaining the contact information of certain clients.
   *  The first page must contain a quick view of the contact information of clients. 
5. Editing the profile of certain clients.
   *  Auto saving (for outdoor recording)

## **Blockchain (Sijiang Guo, Siming):**

Duplicated from: https://gitlab.cecs.anu.edu.au/u5804087/NPF/issues/17

* Introductory demo of Blockchain in the case of caseworker taking information from a new client (original): 
[Introductory_Demo_of_Blockchain.pptx](/uploads/b8c643a931dc9b09e865994f25ddf287/Introductory_Demo_of_Blockchain.pptx)

*  Coding examples (online source):
  https://blockgeeks.com/guides/blockchain-coding/

*  Basic coding steps:
  1.  Create Genesis block: the start of a blockchain
  2.  Add a new block: contain all newly and previously added records in a cryptographic format.
  3.  Validate all blocks (ensure no change has been made to previous blocks). 

*  Pros
  1.  Transparency and immutability. Inherently open, incorruptible and unchangeable.
  2.  Foundational technology. The world sees a trend of widely and deeply adopting this technology.
  3.  Widely available and accessible for different platforms/systems.

*  Cons
  1.  Costly. Relatively high initial cost.
  2.  High energy consumption. Requires high storing space and high computational power for validation.
  3.  Lower performance than the centralised database.

*  Conclusion

Blockchain may not be the most suitable technology for this project since it is a not-for-profit project. But given sufficient computational power, it is a good chance to adopt this new technology.

## **User Interface Design Rules (Shixian Li):**

Duplicated from: https://gitlab.cecs.anu.edu.au/u5804087/NPF/issues/27

**Step 1: app navigation and exploration**
* Show the value of the app upfront. (figure 1)
* Organize and label menu categories to be user-friendly. (-figure 2)
* Allow users to "go back" easily in one step. (- figure 3)
* Make it easy to manually change location. (-figure 4)
* Create frictionless transitions between mobile apps and the mobile web. (-figure 5)

**Step 2: In-App Search**
* Prominently display the search field. (-figure 6)
* Use effective search indexing. (-figure 7)
* Provide filter and sort options. (-figure 8)

**Step3: commerce and conversions**
* Provide previous search and purchase information. (-figure 9)
* Allow user reviews to be viewed and filtered. (-figure 10)
* Enable comparison shopping features. (-figure 11)
* Provide multiple third-party payment options. (-figure 12)
* Make it easy to add and manage payment methods. (-figure 13)

**Step 4: Registration**
* Provide clear utility before asking users to register. (-figure 14)
* Differentiate "sign in" from "sign up."(-figure 15)
* Make password authentication a frictionless experience. (-figure 16)

**Step 5: From Entry**
* Build user-friendly forms. (-figure 17)
* Communicate form errors in real time. (-figure 18)
* Match the keyboard with the required text inputs. (-figure 19)
* Provide helpful information in context in forms. (-figure 20)

**Step 6: Usability and Comprehension**
* Speak the same language as your users. (-figure 21)
* Provide text labels and visual keys to clarify visual information. (-figure 22)
* Be responsive with visual feedback after significant actions. (-figure 23)
* Let the user control the level of zoom. (-figure 24)
* Ask for permissions in-context. (-figure 25)

Reference : Jenny Gov from google.
https://www.thinkwithgoogle.com/marketing-resources/experience-design/principles-of-mobile-app-design-introduction/ 


Further More, To have a clear design, the following things need to be considered:

1.Line spacing and paragraph spacing:

![image](/uploads/2906c7125561ee2b950d1bd4ac2ffa06/image.png)





















Pay attention to the elements, make them look more easier to recognize: 
![image](/uploads/86e35dec2c0bb296c22f223570f2650b/image.png)

 






Negative space is important things as well: 

![image](/uploads/04aa2540aeaa900417b3d449bc918c9c/image.png)

## **Google Glass (Zongyuan (Simon) Wang)**

Background,  Google Glass OCR research and evaluation.

Google Glass.

Traditional: has already stopped manufacture

Enterprise edition. 1800 US dollar. According to today’s (15/4/2018) exchange rate. The price is equivalent to 2354 AUD. 

**Advantage of Google Glass OCR.**

1.	Light and portable.
2.	High technology.

**Disadvantage of Google Glass OCR.**

1.	5 hour of battery life, low on endurance
2.	Google Glass does not contain OCR API. It needs web API to process the image.
3.	Roughly 2354 AUD, which is overpriced.

However we could use Android phone OCR. Android system has mature technology about the  OCR

https://betanews.com/2017/08/08/glass-enterprise-edition-price/
https://www.techradar.com/reviews/gadgets/google-glass-1152283/review 
https://stackoverflow.com/questions/19186347/is-there-an-ocr-api-for-google-glass
