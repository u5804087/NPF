# ATTENTION: THIS LANDING PAGE IS OUTDATED! CURRENT PAGE: 
https://gitlab.cecs.anu.edu.au/u5561598/Digital-NPFs-Semester

# TechLauncher Project - Digital NFPs
# Digital NFPs
![alt text](https://i.imgur.com/GWw19g0.png) ![alt text](https://i.imgur.com/HFvePCg.png)


[**Main Folder**] (https://drive.google.com/drive/folders/1jOXzn_y1YBfBTKi_ZMMV78d06cQHPrOi?usp=sharing)

Main Google drive folder. It contains all the files that generated during the project.


[**Project Overview**] 
(https://docs.google.com/document/d/1wa0cjpBdpVz0Jxn3gJtini6T7N9tpcBGdSd9w1X9YsM/edit?usp=sharing)

Gives an overview of the project, most of the important information is located here 

## Table of Contents
[Documentation](#documentation)

[Introduction](#introduction)

[The Project](#the-project)

[About This Team](#about-this-team)

[Project Milestones](#project-milestones)

[Issue Tracking](#issue-tracking)

## Documentation
Here are all the documents generated during the group meeting.

**Week 1.**
 - This week the team was assembled. 

[**Week 2**]
(https://drive.google.com/drive/folders/19l7trmdK3NcHd2u5dHRnIA_IJmdpY2XV?usp=sharing)
- This week we had our first team meeting with SAP (26th Feburary) and an additional team meeting without SAP to prepare for our first audit (3rd March).
- This week was preoccupied with us understanding the problem, understanding TechLauncher and preparing for our first audit.

[**Week 3**]
(https://drive.google.com/drive/folders/1yKSUN_CrmqGDrB_M1FOnGUMmvmtTWTuu?usp=sharing)
- This week was the week of the first audit.
- This week we had a team meeting with SAP (March 5th) and the first audit tutorial (March 9th).
- This week we primarily focused on preparing for the audit presentation by understanding the project in as much depth as possible.
- Work relating to the Week 3 audit can be found [here.](https://drive.google.com/open?id=14Z2L7eos0fX29tt04L-CaIC2qrsfF_5f)

**Week 4**
- Due to the Canberra Day public holiday, we were unable to meet with representatives from SAP this week on Monday. Due to the status of the project, we decided that scheduling another meeting was unnecessary. There was no tutorial this week.
- Week 4 primarily focused on reading through and understanding the work done the previous years and polishing the presentation for Audit 1. While we didn't produce any significiant documentation this week, the work conducted this week was important for our group's complete understanding of the project.

[**Week 5**]
(https://drive.google.com/drive/folders/1lB1zLf6aLvQieoug_98Ok42rRZlLeTeS?usp=sharing)
- This week we had a team meeting with SAP (March 19th), a [design thinking session with SAP (March 22nd).](https://drive.google.com/drive/folders/1E2CdZ7avyo8xEYyykjD96VWdK6W_lxnd?usp=sharing) and a tutorial (March 23rd).
- During the team meeting we met with the CBE students for the first time.
- The ideas discussed during the design thinking session made up the basis of our GitLab issues.
- We also spent time preparing for Week 6's audit.

[**Week 6**]
(https://drive.google.com/drive/folders/18LouJv8aSrtIMCO2qDRndYJF478sL1En?usp=sharing)
- This week we had a team meeting with SAP (March 26th) and the second audit tutorial (March 29th).
- In our team meeting, we where we were up to and what kind of work we would do over the term break, including creating the first iteration of our Build.me forms.
- This week was primarily focused on preparing for the audit as we decided that student schedules (exams and assignments) did not permit for significantly more work.
- Work relating to the Week 6 audit can be found [here.](https://drive.google.com/drive/folders/1F-DEizHeS9kAoFxAgrRIfntcOWydXUd0?usp=sharing)

[**Term Break Week 1**]
(https://drive.google.com/open?id=19ClxjUDJFvfBuFOPJUpPXvZXEZ2gI0rz)
- This week we had a team meeting with SAP on the 4th of April.
- We discussed how the audit went and how progress on the work assigned to us was going.

[**Term Break Week 2**]
(https://drive.google.com/open?id=1Imx0zcZT623f3CYlvgAkE2FiExdPmDYl)
- This week we had a team meeting with SAP on the 10th of April.
- We discussed doing research into technologies that could benefit the system we are designing.
- We presented the first iteration of our Build.me form to SAP and discussed what changes would need to be made before presenting to clients.

[**Week 7**]
(https://drive.google.com/open?id=1rzDtAXBEFcSJx9Q5IORl4hceZb6L8TFI)
- This week we had a team meeting with SAP on the 16th of April and a tutorial on the 20th of April.
- This week we discussed the findings of the research into UI/UX design, Blockchain and Google Glasses.
- Our research on these technologies can be found in our [Knowlege Hub.](https://gitlab.cecs.anu.edu.au/u5804087/NPF/blob/master/knowledge_hub.md)
- The second iteration of the Build.me form was completed this week. Redacted images of the form can be found [here](https://drive.google.com/open?id=1NpzY0c3TePVkMiaOgf5kj68LPcJsgmik)

[**Week 8**]
(https://drive.google.com/open?id=1FmOxbAD-Tln1-qW0QYM2m8UAzMsoXwhz)
- This week we had a team meeting with SAP on the 23rd of April. There was no tutorial this week.
- We had the first face-to-face meeting with our not-for profit client on the 26th of April. They were impressed with our work and gave us valuable feedback that can be viewed in the Week 8 folder.
- Work began on creating the poster this week with team members attending the Poster workshop on the 24th of April.

[**Week 9**]
(https://drive.google.com/open?id=1PgOBGIfpQmaaYDbFF9vvdvUB5BRxLVbh)
- This week we had a team meeting with SAP on the 30th of April and a tutorial on the 4th of May.
- We finished the poster for the Week 10 showcase. Unfortunate circumstances meant we would have to revise the poster next week. The poster can be downloaded [here.](https://drive.google.com/drive/folders/1PgOBGIfpQmaaYDbFF9vvdvUB5BRxLVbh?usp=sharing)
- We completed the first iteration of the UML diagram of our backend. This will be improved upon in further iterations. It can be found [here.](https://drive.google.com/file/d/1aAGPxLPz0YWZImZQayI7xAJP_PjT3Q16/view?usp=sharing)
- We created a decision table, compiling all of our decisions across the semester. It can be found [here.](https://docs.google.com/document/d/1RJ1dtMX9Z5jrn7xZ1pBbJ3cxXLbos6R_dxSdEZxmlDc/edit?usp=sharing)

[**Week 10**]
(https://drive.google.com/open?id=1BVMBBXbYQM4vzGYoDwNH8X-zsia5QIEm)
- This week we had a team meeting with SAP on the 7th of April and the TechLauncher showcase on the 8th of April. We will have a Audit Tutorial on the 11th of April.
- During the team meeting with SAP, we had to revise the poster for the showcase. The new poster can be found [here.](https://drive.google.com/open?id=1BVMBBXbYQM4vzGYoDwNH8X-zsia5QIEm)
- With assistance from Alan Bradbury, we created a website for our team. The website can be found [here.](https://digitalnfpsolutions.com/)

[**Audits**] 
(https://drive.google.com/drive/folders/14Z2L7eos0fX29tt04L-CaIC2qrsfF_5f?usp=sharing)
- [Audit 1 presentation ppt] (https://docs.google.com/presentation/d/1YwogwxvxUW88rhzhTB3En5yL6m4GFhG-ntaHA1K9O6w/edit?usp=sharing)
- [Audit 2 presentation ppt] (https://docs.google.com/presentation/d/1QxlbTuuzWrfdHozBkEQ89ckD18yWad6tJDM9KEw4-bI/edit?usp=sharing)
- Audit 3 presentation will be completed before Friday.

[**Our Poster**]
(https://drive.google.com/open?id=1BVMBBXbYQM4vzGYoDwNH8X-zsia5QIEm)

[**CBE Projects**]
(https://drive.google.com/drive/folders/1ZXFc_P3jR7L4Go1VlQbF25XRudrfT5rC?usp=sharing)




## **Introduction**

This document aims to give a brief overview of this team&#39;s project to demonstrate their understanding of the project and their readiness to proceed for the semester. Additionally, this document will prove that this team has considered important details such as meeting times, project milestones and team organisation.

## **The Project**

This team is working together with SAP SE to improve the recording system that the not for profit (NFP) client uses for record keeping. At this stage the client has requested anonymity so the initials KH will be used to reference the client in the document.

The NFP is a Canberra-based charity that assists women in crisis. The team&#39;s understanding is that they are having many difficulties with their current record-keeping systems. They have multiple systems for collecting data for their clients, some of which are paper-based. Due to the organisation of their systems, they often need to enter the same data manually into multiple systems which is extremely time consuming. The paper aspects of the system produces additional challenges; the paper that they use is extremely easy to lose or misplace. Their paper system is also insecure. Their paper system also takes up a large amount of physical space, making it difficult to review client data. This problem is exacerbated by multiple systems that do not interface with each other well, meaning that client information could be spread across multiple systems. Client information must be retrieved to create reports that are legally required by the government, consuming a great deal of time for KH.

The team&#39;s understanding of the project&#39;s primary objective is to reduce the amount of time KH spend on administrative tasks, freeing up time for other tasks. Ways that time could be saved on these tasks include reducing the amount of data entry required, reducing the difficulty of government reporting and making it easier to access client information. A secondary benefit of this project is increased security of the system.

This project has been operating since mid-2016. It has seen input from SAP SE and other ANU students from varying disciplines. This team will be using information found by these people during the course of this project. The first team attempted to review longitudinal data sets. The second team reviewed the data available in spreadsheet form and presented visual representations and the third team wrote a business case to support the digitalisation of current paper-based systems

## **About This Team**

The 2018 semester one team consists of five students in TechLauncher. The team has listed the degrees that they are pursuing and the provisional roles they will be taking in this project. These roles may change due to expansion or restriction of the scope, development of new skills or unforeseen circumstances.

- Sijiang Guo - u5626799
  - Bachelor of Engineering (Honours), Major in Mechatronics Engineering.
  - System Designer
- Siming Li - u5466774
  - Bachelor of Information technology, Major in Software development.
  - Bachelor of Commerce, Major in Accounting
  - Project management, Programmer, Client Liaison
- Shixian Li - u5794589
  - Bachelor of Engineering (Honours), Major in Electronic Engineering.
  - Administrator
- Zongyuan Wang u5804087
  - Bachelor of Engineering (Honours), Major in Electronic Engineering.
  - Administrator
- Lawrence Wooding - u5561598
  - Bachelor of Software Engineering (Honours)
  - Head Programmer


This team is working closely with the following people at SAP SE. Eventually, this project will involve a formal presentation to SAP SE Executives.

- Alan Bradbury - Industry Value Engineer
  - Mentor and strategist
- Cathy McGurk - Director, Customer Solutions
  - Mentor, Client liaison, document and project review
- Michael Redman - Enterprise Architect
  - Michael is the technical representative at SAP SE - he has a development background and will support the use of SAP SE technologies

This team is also working with two students from the ANU College of Business and Economics.
- Eunji Jeon u6028447
- Danni Liu  u6031254

## **Project Milestones**

On the 22nd of March 2018, this team adjusted their project milestones after consulting client. The new milestones are below:

- 29th of March - 2nd Audit
- (Now - 5th of April) - Understand all technologies involved in the project (e.g Build), perform all pre-administrative actions needed.
- (5th of April - 26th of April) - Develop first iteration of the prototype for internal review and review by clients, develop presentation for clients describing the prototype.
- (26th of April - 2nd of May)  - Polish the presentation for the clients.
- 2nd of May - Presentation with the clients
- 7th of May - Final Audit
- 8th of May - Project Showcase
- 11th of May - Final Presentation to SAP Executives

These milestones were met. The presentation for the clients happened one week early due to meeting of milestones earlier than first predicted.

The earlier, less detailed version of these milestones for the semester can be viewed below:

- End of Week 4 - Studied the previous data gathered by those previously working on this project, choose which technologies to use in solution after consultation with stakeholders.
- End of Week 6 - Making proof of concept system for review by stakeholders.
- End of Week 10  - Have an improved system after having the stakeholders review system.

## **Issue Tracking**

During our design thinking session on the 22nd of May, we assigned tasks to each person. We attempted to translate this into the Issue system of GitLab, however we discovered that this platform has certain limitations, namely that it is impossible to assign more than one person to a task. To work around this, we have given each team member labels and put their labels on the tasks that they are assigned to. If this proves to restricting, we will consider our issue tracking to another platform.
